import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Home(),
    title: "Contador de Pessoas",
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _pessoas = 0;
  String _infoText = "Tá de boa.";

  void _chancePessoas(int quantidade) {
    setState(() {
      _pessoas += quantidade;

      if (_pessoas < 0)
        _infoText = "Tá bem loko!";
      else if (_pessoas <= 10)
        _infoText = "Tá de boa.";
      else
        _infoText = "Tá socadasso.";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Image.asset("images/fundo.jpg", fit: BoxFit.cover, height: 1000),
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Pessoas: $_pessoas",
            style: TextStyle(
                color: Colors.yellowAccent, fontWeight: FontWeight.bold),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Padding(
                padding: EdgeInsets.all(10),
                child: TextButton(
                    onPressed: () {
                      _chancePessoas(-1);
                    },
                    child: Text(
                      "-1",
                      style: TextStyle(fontSize: 40, color: Colors.black87),
                    ))),
            Padding(
                padding: EdgeInsets.all(10),
                child: TextButton(
                    onPressed: () {
                      _chancePessoas(1);
                    },
                    child: Text(
                      "+1",
                      style: TextStyle(fontSize: 40, color: Colors.black87),
                    ))),
          ]),
          Text(
            _infoText,
            style: TextStyle(
                color: Colors.yellow,
                fontStyle: FontStyle.italic,
                fontSize: 30),
          )
        ],
      )
    ]);
  }
}
