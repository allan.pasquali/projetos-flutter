import 'package:flutter/material.dart';
import 'package:loja_virtual/Tabs/categoriesTab.dart';
import 'package:loja_virtual/Tabs/homeTab.dart';
import 'package:loja_virtual/Tabs/widgets/customDrawer.dart';

class HomeScreen extends StatelessWidget {
  final _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: [
        Scaffold(
          body: HomeTab(),
          drawer: CustomDrawer(_pageController),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text("Produtos"),
            centerTitle: true,
          ),
          drawer: CustomDrawer(_pageController),
          body: CategoriesTab(),
        ),
        Container(
          color: Colors.green,
        ),
      ],
    );
  }
}
