import 'package:cloud_firestore/cloud_firestore.dart';

class ProductData {
  String category;
  String id;
  String title;
  String description;
  double price;
  String priceFormat;
  List image;
  List size;

  ProductData.fromDocument(DocumentSnapshot snapshot) {
    id = snapshot.documentID;
    title = snapshot.data["title"];
    description = snapshot.data["description"];
    price = double.tryParse(snapshot.data["price"].toString());
    image = snapshot.data["image"];
    size = snapshot.data["size"];
  }
}
