import 'package:chat_flutter/chat_screenList.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          iconTheme: IconThemeData(color: Colors.blueAccent)),
      home: ChatScreen(),
    );
  }
}


  // Firestore.instance
  //     .collection("mensagens")
  //     .document()
  //     .setData({"texto": "Olá", "from": "Allan", "read": false});

  // QuerySnapshot snapshot =
  //     await Firestore.instance.collection("mensagens").getDocuments();
  // snapshot.documents.forEach((element) {
  //   element.reference.updateData({"lido": "false"});
  //   print(element.data);
  //   print(element.documentID);
  // });

  // DocumentSnapshot snapshotDocument =
  //     await Firestore.instance.collection("mensagens").document("msg1").get();
  // print("Aqui documento: ${snapshotDocument.data}");
  // print("Aqui documento: ${snapshotDocument.documentID}");

  // Firestore.instance.collection("mensagens").snapshots().listen((dado) {
  //   dado.documents.forEach((element) {
  //     print(element.data);
  //   });
  // });
  
  // Firestore.instance
  //     .collection("mensagens")
  //     .document("msg1")
  //     .snapshots()
  //     .listen((dado) {
  //   print(dado.data);
  // });
