import 'package:flutter/material.dart';

class ChatMessage extends StatelessWidget {
  ChatMessage(this.data, this.mine);

  final Map<String, dynamic> data;
  final bool mine;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Row(
        children: [
          !mine
              ? Padding(
                  padding: const EdgeInsets.only(right: 16),
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(data['senderPhotoUrl']),
                  ))
              : Container(),
          Expanded(
              child: Column(
            crossAxisAlignment:
                mine ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            children: [
              data['imgUrl'] != null
                  ? Image.network(
                      data['imgUrl'],
                      width: 250,
                    )
                  : Card(
                      margin: mine
                          ? EdgeInsets.only(left: 40)
                          : EdgeInsets.only(right: 40),
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          data['text'],
                          textAlign: mine ? TextAlign.end : TextAlign.start,
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ),
                      elevation: 2,
                      borderOnForeground: true,
                      color: mine ? Colors.grey[300] : Colors.blueGrey[300],
                    ),
              Padding(
                padding: EdgeInsets.only(top: 5),
                child: Text(
                  data['senderName'],
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                ),
              )
            ],
          )),
          mine
              ? Padding(
                  padding: const EdgeInsets.only(left: 16),
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(data['senderPhotoUrl']),
                  ))
              : Container(),
        ],
      ),
    );
  }
}
