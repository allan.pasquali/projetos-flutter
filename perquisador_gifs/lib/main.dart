import 'package:flutter/material.dart';
import 'package:perquisador_gifs/ui/gif_page.dart';
import 'package:perquisador_gifs/ui/home_page.dart';

void main() {
  runApp(MaterialApp(
    home: HomePage(),
    theme: ThemeData(hintColor: Colors.white),
  ));
}
